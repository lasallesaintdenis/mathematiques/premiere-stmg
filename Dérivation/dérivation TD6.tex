\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{Dérivation}
\author{1\up{ère}STMG}
\date{}

\begin{document}

\maketitle\\


On considère une fonction $f$ définie et dérivable sur l'intervalle $[0~;~\np{2,5}]$.

On note $f'$ la fonction dérivée de la fonction $f$.

On donne \textbf{en annexe}, la courbe représentative de la fonction $f$, appelée $\mathcal{C}$, dans un repère orthogonal.

La courbe $\mathcal{C}$ possède les propriétés suivantes :

\setlength\parindent{5mm}
\begin{itemize}
\item la courbe $\mathcal{C}$ passe par le point $A(1~;~\np{5,5})$ ;
\item la courbe $\mathcal{C}$ passe par le point $B(2~;~2)$ ;
\item la tangente en $B$ à  la courbe $\mathcal{C}$ est horizontale ;
\item la tangente en $A$ à  la courbe $\mathcal{C}$ passe par le point $T(0~;~\np{8,5})$.
\end{itemize}
\setlength\parindent{0mm}

\smallskip

\textbf{Partie I}

\medskip

\begin{enumerate}
\item Placer les points $A$, $B$ et $T$ et tracer les tangentes à  la courbe $\mathcal{C}$ en $A$ et $B$.
\item Déterminer $f(1)$, $f(2)$ et $f'(1)$.
\item Donner par lecture graphique une valeur approchée des solutions de l'équation $f(x) = 3$.
\item Justifier que $f'(2) = 0$. 
\item Donner par lecture graphique une valeur approchée de la deuxième solution de l'équation $f'(x) = 0$.
\end{enumerate}
\smallskip

\textbf{Partie II}

\smallskip

La fonction $f$ dont on connaît la courbe $\mathcal{C}$ est définie sur l'intervalle $[0~;~\np{2,5}]$ par :

\[f(x) = 4x^3 - \np{16,5}x^2 + 18x.\]

\begin{enumerate}
\item Compléter le tableau de valeurs suivant à  l'aide de la calculatrice.

\begin{center}
\begin{tabularx}{\linewidth}{|*{7}{>{\centering \arraybackslash}X|}}
\hline
$x$ 	&0 	&\np{0,5} 	&1 	&\np{1,5} 	&2 	&\np{2,5} \\ \hline
$f(x)$ 	& 	& 			& 	& 			& 	& \\ \hline
\end{tabularx}
\end{center}

\item 
	\begin{enumerate}
		\item Calculer $f'(x)$.
		\item Montrer que: $f'(x)=(12x - 24)(x -\np{0,75})$.
		\item \'Etudier le signe de $f'(x)$ suivant les valeurs de $x$ sur l'intervalle $[0~;~\np{2,5}]$ à  l'aide d'un tableau de signe.
	\end{enumerate}
\item En déduire le tableau de variations de $f$.
\end{enumerate}
\begin{center}
\begin{tikzpicture}[scale=2.5]
\tkzInit[xmin=0,xmax=3,ymin=0,ymax=9]
\tkzAxeXY
\draw[gray, dotted, xstep=0.1,ystep=0.25](0,0)grid(3,9);
\draw[red,very thick,domain=0:2.8,samples=200] plot (\x,{4*\x*\x*\x-16.5*\x*\x+18*\x});
\end{tikzpicture}
\end{center}
\end{document}




